@extends('layouts.app')

@section('content')

    <p class="card-text h4">Автор: <a href="{{route('users.show', ['user' => $photo->user])}}">{{$photo->user->name}} {{$photo->user->surname}}</a></p>

    <div class="card" style="width: 40rem;">
        <img src="{{asset('storage/' . $photo->photo)}}" alt="{{$photo->photo}}" class="card-img-top" style="height: 500px">
        <div class="card-body text-right">
            <p class="card-text">{{$photo->description}}</p>
            <p class="card-text text-right"><small class="text-muted">{{$photo->created_at->diffForHumans()}}</small></p>
            @can('delete', $photo)
            <form class="mt-3" method="post" action="{{route('photos.destroy', ['photo' => $photo])}}">
                @method('delete')
                @csrf
                <button type="submit" class="btn btn-outline-danger btn-sm">Удалить</button>
            </form>
            @endcan
            <form action="{{route('likes.store')}}" method="post">
                @csrf
                <input type="hidden" id="photo_id" name="photo_id" value="{{$photo->id}}">
                <button type="submit" class="border-0" style="font-size:20px;"><i class='far fa-thumbs-up'></i></button>
            </form>
            <div>
                @foreach($photo->likes as $photo_like)
                    @can('delete', $photo_like)
                        <form action="{{route('likes.destroy', ['like' => $photo_like])}}" method="post" style="margin-top: -34px">
                            @method('delete')
                            @csrf
                            <button type="submit" class="border-0" style="font-size:20px; "><i class='fas fa-thumbs-up'></i></button>
                        </form>
                    @endcan
                @endforeach

            </div>
            <p>{{$photo->likes->count()}}</p>
        </div>
    </div>

    <div>
        <button class="show-comments-btn btn btn-info mt-4" type="button" data-toggle="collapse"
                data-target="#comments-block">Комментарии: <span class="comments-number">{{$comments_count}}</span></button>
        <div id="comments-block" class=" collapse multi-collapse mt-5">
            @foreach($photo->comments as $comment)
                @can('show', $comment)
                <div class="card mb-3 w-50">
                    <h5 class="card-header">{{$comment->user->surname}} {{$comment->user->name}}</h5>
                    <div class="card-body">
                        <p class="card-text">{{$comment->body}}</p>
                        <p class="card-text text-right"><small class="text-muted">{{$comment->created_at->diffForHumans()}}</small></p>
                            <hr>
                            <div>
                                @can('edit', $comment)
                                <p><a href="{{route('comments.edit', ['comment' => $comment])}}">Изменить комментарий</a></p>
                                @endcan
                                @can('delete', $comment)
                                <form class="mt-3" method="post" action="{{route('comments.destroy', ['comment' => $comment])}}">
                                    @method('delete')
                                    @csrf
                                    <input type="hidden" id="photo_id" name="photo_id" value="{{$photo->id}}">
                                    <button type="submit" class="btn btn-outline-danger btn-sm">Удалить</button>
                                </form>
                                @endcan
                            </div>
                    </div>
                </div>
                @endcan
            @endforeach
        </div>
    </div>

    @can('showCommentForm', $photo)
    <form method="post" action="{{route('comments.store')}}" class="w-25 mt-5 mb-5">
        @csrf
        <input type="hidden" id="photo_id" name="photo_id" value="{{$photo->id}}">
        <div class="form-group">
            <label for="body">Комментарий:</label>
            <textarea class="form-control @error('body') is-invalid @enderror" id="body" name="body" rows="3"></textarea>
            @error('body')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <button type="submit" class="btn btn-success">Добавить комментарий</button>
    </form>
    @endcan

@endsection
