@extends('layouts.app')

@section('content')

    <h2 class="text-center">Лента</h2>

    <div class="row row-cols-1 row-cols-md-3 mt-4">
        @foreach($photos as $photo)
            <div>
                <p>Автор: <a href="{{route('users.show', ['user' => $photo->user->id])}}">{{$photo->user->name}} {{$photo->user->surname}}</a></p>
                <a href="{{route('photos.show', ['photo' => $photo])}}">
                    <img src="{{asset('storage/' . $photo->photo)}}" alt="{{$photo->photo}}" class="mb-5" style="width: 300px; height: 250px">
                </a>

                <div>
                    <form action="{{route('likes.store')}}" method="post" style="margin-top: -30px">
                        @csrf
                        <input type="hidden" id="photo_id" name="photo_id" value="{{$photo->id}}">
                        <button type="submit" class="border-0" style="font-size:20px;"><i class='far fa-thumbs-up'></i></button>
                    </form>
                </div>

                <div>
                    @foreach($photo->likes as $photo_like)
                        @can('delete', $photo_like)
                        <form action="{{route('likes.destroy', ['like' => $photo_like])}}" method="post" style="margin-top: -34px">
                            @method('delete')
                            @csrf
                            <button type="submit" class="border-0" style="font-size:20px; "><i class='fas fa-thumbs-up'></i></button>
                        </form>
                        @endcan
                    @endforeach

                </div>
                <p>{{$photo->likes->count()}}</p>
                <hr>
            </div>
        @endforeach
    </div>

    <div class="row p-5">
        <div class="col-12">
            {{ $photos->links() }}
        </div>
    </div>

@endsection
