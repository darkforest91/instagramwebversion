@extends('layouts.app')

@section('content')

    <h1>Добавить фотографию</h1>

    <form method="post" action="{{route('photos.store')}}" class="w-50" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="photo">Фотография</label>
            <input type="file" class="form-control-file @error('photo') is-invalid @enderror" id="photo" name="photo">
            @error('photo')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="form-group">
            <label for="description">Описание фотографии</label>
            <textarea class="form-control @error('description') is-invalid @enderror" id="description" rows="3" name="description"></textarea>
            <small class="form-text text-muted">
                *Заполнять не обязательно.
            </small>
            @error('description')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">Добавить</button>
    </form>

    <div class="mt-3 mb-3">
        <a href="{{route('users.index')}}">Назад</a>
    </div>

@endSection
