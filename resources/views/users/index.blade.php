@extends('layouts.app')

@section('content')

    <div class="text-right">
        <h2>{{Auth::user()->name}} {{Auth::user()->patronymic}} {{Auth::user()->surname}}</h2>
        <p><a href="{{route('photos.create')}}">Добавить фото</a></p>
    </div>
    <p class="mt-3 h4">Ваши фотографии:</p>

    <div>
        @foreach(Auth::user()->photos as $photo)
            <a href="{{route('photos.show', ['photo' => $photo])}}"><img src="{{asset('storage/' . $photo->photo)}}" alt="{{$photo->photo}}" class="img-thumbnail" style="width: 300px; height: 250px"></a>
        @endforeach
    </div>

@endsection
