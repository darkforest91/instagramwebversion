@extends('layouts.app')

@section('content')

    <div class="text-right">
        <h2>{{$user->name}} {{$user->patronymic}} {{$user->surname}}</h2>

        @can('create', $user)
            <form method="post" action="{{route('subscriptions.store')}}">
                @csrf
                <input type="hidden" id="user_id" name="user_id" value="{{$user->id}}">
                <button type="submit" class="btn btn-primary small">Подписаться</button>
            </form>
        @endcan

        @can('delete', $user)
        <form method="post" action="{{route('subscriptions.destroy', ['subscription' => $user])}}">
            @method('delete')
            @csrf
            <button type="submit" class="btn btn-danger small">Отписаться</button>
        </form>
        @endcan
    </div>

    <div>
        <h3>Все фотографии</h3>
        <hr>
        <div class="row row-cols-1 row-cols-md-3 mt-4">
            @foreach($user->photos as $photo)
                <div>
                    <a href="{{route('photos.show', ['photo' => $photo])}}">
                        <img src="{{asset('storage/' . $photo->photo)}}" alt="{{$photo->photo}}" class="mb-5 mr-5" style="width: 300px; height: 250px">
                    </a>

                    <div>
                        <form action="{{route('likes.store')}}" method="post" style="margin-top: -30px">
                            @csrf
                            <input type="hidden" id="photo_id" name="photo_id" value="{{$photo->id}}">
                            <button type="submit" class="border-0" style="font-size:20px;"><i class='far fa-thumbs-up'></i></button>
                        </form>
                    </div>

                    <div>
                        @foreach($photo->likes as $photo_like)
                            @can('delete', $photo_like)
                                <form action="{{route('likes.destroy', ['like' => $photo_like])}}" method="post" style="margin-top: -34px">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="border-0" style="font-size:20px; "><i class='fas fa-thumbs-up'></i></button>
                                </form>
                            @endcan
                        @endforeach
                    </div>
                    <p>{{$photo->likes->count()}}</p>
                    <hr>
                </div>
            @endforeach
        </div>
    </div>

@endsection
