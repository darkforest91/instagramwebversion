@extends('layouts.app')

@section('content')

    <div class="text-center mt-5">
        <h1>Привет, {{Auth::user()->name}}.</h1>
        <p class="h2 mt-5">Добро пожаловать в <b>Instagram</b></p>
    </div>

@endsection
