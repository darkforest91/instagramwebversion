@extends('layouts.app')

@section('content')

    <h3>Изменить комментарий</h3>

    <form method="post" action="{{route('comments.update', ['comment' => $comment])}}" class="w-25 mt-5 mb-5">
        @method('put')
        @csrf
        <input type="hidden" id="photo_id" name="photo_id" value="{{$comment->photo_id}}">
        <div class="form-group">
            <label for="body">Комментарий:</label>
            <textarea class="form-control @error('body') is-invalid @enderror" id="body" name="body" rows="3">{{$comment->body}}</textarea>
            @error('body')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <button type="submit" class="btn btn-success">Изменить</button>
    </form>

    <div class="mt-3 mb-3">
        <a href="{{route('photos.show', ['photo' => $comment->photo_id])}}">Назад</a>
    </div>

@endsection
