<?php

namespace App\Orchid\Layouts;

use App\Comment;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CommentListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'comments';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('user_id', 'Автор комментария')
                ->sort()
                ->render(function (Comment $comment) {
                    return "{$comment->user->name} {$comment->user->surname}";
                }),
            TD::set('show', 'Просмотр комментария')
                ->render(function (Comment $comment) {
                return Link::make()->icon('icon-envelope')->route('platform.comments.edit', $comment);
            }),
            TD::set('is_active', 'Активность')
                ->sort()->render( function (Comment $comment) {
                    if ($comment->is_active) {
                        return Link::make()->icon('icon-check')->route('platform.comments.edit', $comment);
                    } else {
                        return Link::make()->icon('icon-circle_thin')->route('platform.comments.edit', $comment);
                    }

                }),
            TD::set('created_at', 'Создан')->sort(),
            TD::set('updated_at', 'Изменен')->sort()
        ];
    }
}
