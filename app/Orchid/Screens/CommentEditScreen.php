<?php

namespace App\Orchid\Screens;

use App\Comment;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class CommentEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование комментария';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Comment $comment): array
    {
        return [
            'comment' => $comment
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Редактирование комментария')
                ->icon('icon-wrench')
                ->method('update'),
            Button::make('Удалить комментарий')
                ->icon('icon-trash')
                ->method('remove')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([

                Input::make('comment.user.name')
                    ->title('Имя пользователя')
                    ->readonly(),
                Input::make('comment.user.surname')
                    ->title('Фамилия пользователя')
                    ->readonly(),
                CheckBox::make('comment.is_active')
                    ->title('Активность комментария')
                    ->sendTrueOrFalse(),
                TextArea::make('comment.body')
                    ->rows(10)
                    ->title('Комментарий')
                    ->required(),
            ])
        ];
    }

    /**
     * @param Comment $comment
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Comment $comment, Request $request)
    {
        $comment->fill($request->get('comment'))->save();
        Alert::info('Успешное редактирование комментария');
        return redirect(route('platform.comments.list'));
    }

    /**
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function remove(Comment $comment)
    {
        $comment->delete()
            ? Alert::info('Комментарий удален')
            : Alert::warning('Комментарий не удален');
        return redirect(route('platform.comments.list'));
    }
}
