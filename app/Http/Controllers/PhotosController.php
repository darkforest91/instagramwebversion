<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhotoRequest;
use App\Photo;
use Illuminate\Http\Request;

class PhotosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $photos = Photo::Paginate(12);
        return view('photos.index', compact('photos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('photos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PhotoRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PhotoRequest $request)
    {
        $photo = new Photo($request->all());
        $photo['user_id'] = $request->user()->id;
        $file = $request->file('photo');
        $photo['photo'] = $file->store('photos', 'public');
        $photo->save();

        return redirect(route('users.index'))->with('status', 'Фотография добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $photo = Photo::findOrFail($id);

        $comments_count = 0;
        foreach ($photo->comments as $comment) {
            if ($comment->is_active) {
                $comments_count = $comments_count + 1;
            }
        }

        return view('photos.show', compact('photo', 'comments_count'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $photo = Photo::findOrFail($id);
        $this->authorize('delete', $photo);
        $photo->delete();
        return redirect(route('users.index'))->with('status', 'Фотография удалена');
    }
}
