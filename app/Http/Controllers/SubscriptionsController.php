<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class SubscriptionsController extends Controller
{
    /**
     * SubscriptionsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $subscriber = $request->user();
        $subscription = User::find($request->input('user_id'));
        $this->authorize('create', $subscription);
        $subscriber->subscriptions()->attach($subscription->id);

        $user = $subscription;
        return redirect(route('users.show',
                    compact('user')))->with('status', "Вы подписались на пользователя {$user->name} {$user->surname}");
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id, Request $request)
    {
        $subscription = User::findOrFail($id);
        $this->authorize('delete', $subscription);
        $subscriber = $request->user();
        $subscriber->subscriptions()->detach($subscription);

        $user = $subscription;
        return redirect(route('users.show',
            compact('user')))->with('status', "Вы отписались от пользователя {$user->name} {$user->surname}");

    }
}
