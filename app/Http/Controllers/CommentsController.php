<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentRequest;
use App\Photo;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * CommentsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param CommentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CommentRequest $request)
    {
        $comment = new Comment($request->all());
        $comment['user_id'] = $request->user()->id;
        $comment['photo_id'] = $request->input('photo_id');
        $comment->save();
        $photo = $request->input('photo_id');

        return redirect(route('photos.show', compact('photo')))->with('status', 'Комментарий будет добавлен после проверки администратором');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        $comment = Comment::findOrFail($id);
        $this->authorize('edit', $comment);
        return view('comments.edit', compact('comment'));
    }

    /**
     * @param CommentRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(CommentRequest $request, $id)
    {
        $photo = $request->input('photo_id');
        $comment = Comment::findOrFail($id);
        $this->authorize('update', $comment);
        $comment->update($request->all());
        return redirect(route('photos.show', compact('photo')))->with('status', 'Комментарий изменен');
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, Request $request)
    {
        $photo = $request->input('photo_id');
        $comment = Comment::findOrFail($id);
        $this->authorize('delete', $comment);
        $comment->delete();

        return redirect(route('photos.show', compact('photo')))->with('status', 'Комментарий удален');
    }
}
