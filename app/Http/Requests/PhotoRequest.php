<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => 'required|image',
            'description' => 'nullable|max:1000'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'photo.required' => 'Поле не должно быть пустым',
            'photo.image' => 'Неверный формат отографии',
            'description.max' => 'Не более 1000 символов'
        ];
    }
}
