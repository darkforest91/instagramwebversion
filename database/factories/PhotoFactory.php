<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Photo;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

function image_path(int $image_number):string
{
    $path = storage_path() . "/seed_images/" . $image_number . ".jpg";
    $image_name = md5($path) . ".jpg";
    $resize = Image::make($path)->fit(300)->encode('jpg');
    Storage::disk('public')->put('photos/' . $image_name, $resize->__toString());

    return 'photos/' . $image_name;
}

$factory->define(Photo::class, function (Faker $faker) {
    return [
        'photo' => image_path(rand(1, 10)),
        'description' => $faker->text,
        'user_id' => rand(1, 6),
//        'photo' => 'for test',
    ];
});

//   Чтобы прогнать тесты, нужно в PhotoFactory.php закоментировать public function image_path() {} c 10 по 18 строку и
//   22 строку 'photo' => image_path(rand(1, 10)), Раскоментировать 25 строку 'photo' => 'for test'.
