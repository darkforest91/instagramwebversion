<?php

namespace Tests\Feature;

use App\Comment;
use App\Photo;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use RefreshDatabase;

    public $user, $comments, $photo;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        $this->user = factory(User::class)->create();
        $this->photo = factory(Photo::class)->create([
            'user_id' => $this->user->id,
            'photo' => 'testphoto',
            'description' => 'test description'
        ]);
        $this->comments = factory(Comment::class, 5)->create([
            'user_id' => $this->user->id,
            'photo_id' => $this->photo->id,
            'body' => 'test description'
        ]);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function testSuccessCreateComment()
    {
        $this->actingAs($this->user);
        $comment = [
            'user_id' => $this->user->id,
            'photo_id' => $this->photo->id,
            'body' => 'Test description'
        ];
        $response = $this->post(route('comments.store', $comment));
        $response->assertStatus(302);
        $this->assertDatabaseHas('comments', $comment);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function testFailedCreateCommentIfUserNotAuth()
    {
        $comment = [
            'user_id' => $this->user->id,
            'photo_id' => $this->photo->id,
            'body' => 'Test description'
        ];
        $response = $this->post(route('comments.store', $comment));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function testSuccessUpdateComment()
    {
        $this->actingAs($this->user);
        $comment = $this->comments->first();
        $comment->body = 'update';
        $response = $this->put(route('comments.update', ['comment' => $comment]), $comment->toArray());
        $response->assertStatus(302);
        $response->assertRedirect(route('photos.show', ['photo' => $comment->photo]));
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function testFailedUpdateCommentIfUserNotAuth()
    {
        $comment = $this->comments->first();
        $comment->body = 'update';
        $response = $this->put(route('comments.update', ['comment' => $comment]), $comment->toArray());
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function testFailedUpdateCommentIfAnotherUser()
    {
        $another_user = factory(User::class)->create();
        $this->actingAs($another_user);
        $comment = $this->comments->first();
        $comment->body = 'update';
        $response = $this->put(route('comments.update', ['comment' => $comment]), $comment->toArray());
        $response->assertStatus(403);
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function testFailedDeleteCommentIfUserNotAuth()
    {
        $response = $this->delete(route('comments.destroy', ['comment' => $this->comments->first()]));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group comments
     * @return void
     */
    public function testFailedDeleteCommentIfAnotherUser()
    {

        $another_user = factory(User::class)->create();
        $this->actingAs($another_user);
        $response = $this->delete(route('comments.destroy', ['comment' => $this->comments->first()]));
        $response->assertStatus(403);
    }
}
