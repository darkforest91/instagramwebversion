<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public $user;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        $this->user = factory(User::class)->create();
    }

    /**
     * A basic feature test example.
     * @group users
     * @return void
     */
    public function testSuccessGetUsersIndex()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('users.index'));
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @group users
     * @return void
     */
    public function testFailedGetUsersIndexIfUserNotAuth()
    {
        $response = $this->get(route('users.index'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group users
     * @return void
     */
    public function testSuccessGetUsersShow()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('users.show', ['user' => $this->user]));
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @group users
     * @return void
     */
    public function testFailedGetUsersShowIfUserNotAuth()
    {
        $response = $this->get(route('users.show', ['user' => $this->user]));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
