<?php

namespace Tests\Feature;

use App\Photo;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PhotoTest extends TestCase
{
    use RefreshDatabase;

    public $user, $photos;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        $this->user = factory(User::class)->create();
        $this->photos = factory(Photo::class,3)->create([
            'user_id' => $this->user->id,
            'photo' => 'photo'
        ]);
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function testSuccessGetMainPage()
    {
        $this->actingAs($this->user);
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function testFailedGetMainPageIfUserNotAuth()
    {
        $response = $this->get(route('photos.index'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function testSuccessCreatePhoto()
    {
        $this->actingAs($this->user);
        $photo = [
            'photo' => 'photo',
            'user_id' => $this->user->id,
            'description' => 'test description',
        ];
        $response = $this->post(route('photos.store', $photo));
        $response->assertStatus(302);
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function testFailedCreatePhotoIfUserNotAuth()
    {
        $photo = [
            'photo' => 'photo',
            'user_id' => $this->user->id,
            'description' => 'test description',
        ];
        $response = $this->post(route('photos.store', $photo));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function testSuccessDeletePhoto()
    {
        $this->actingAs($this->user);
        $response = $this->delete(route('photos.destroy', ['photo' => $this->photos->first()]));
        $response->assertStatus(302);
        $response->assertRedirect(route('users.index'));
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function testFailedDeletePhotoIfUserNotAuth()
    {
        $response = $this->delete(route('photos.destroy', ['photo' => $this->photos->first()]));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group photos
     * @return void
     */
    public function testFailedDeletePhotoIfAnotherUser()
    {
        $another_user = factory(User::class)->create();
        $this->actingAs($another_user);
        $response = $this->delete(route('photos.destroy', ['photo' => $this->photos->first()]));
        $response->assertStatus(403);
    }
}
