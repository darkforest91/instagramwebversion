<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UsersController@index')->name('home');
Route::resource('photos', 'PhotosController')->except(['edit', 'update']);
Route::resource('users', 'UsersController')->only(['index', 'show']);
Route::resource('comments', 'CommentsController')->only(['store', 'edit', 'update', 'destroy']);
Route::resource('subscriptions', 'SubscriptionsController')->only(['store', 'destroy']);
Route::resource('likes', 'LikesController')->only(['store', 'destroy']);


Auth::routes();

